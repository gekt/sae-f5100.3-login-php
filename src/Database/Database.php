<?php
namespace App\Database;

use PDO;
use PDOStatement;

class Database {
    public string $db_name;
    public string $db_user;
    public string $db_pass;
    public string $db_host;
    public $pdo;

    /**
     * DB constructor.
     */
    public function __construct() {
        $this->db_name = "f3login";
        $this->db_user = "root";
        $this->db_pass = "";
        $this->db_host = "127.0.0.1";
    }

    /**
     * Initialize PDO connection
     * @return PDO
     */
    private function getPDO() {
        if ($this->pdo === null) {
            $pdo = new PDO('mysql:dbname=' . $this->db_name . ';host=' . $this->db_host . '', $this->db_user, $this->db_pass);

            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $pdo->exec('SET NAMES utf8');

            $this->pdo = $pdo;
        }
        return $this->pdo;
    }

    /**
     * Execute simple query BDD without attributes and prepare.
     * @param $statement
     * @return PDOStatement
     */
    public function querySimple($statement) {
        $req = $this->getPDO()->query($statement);
        return $req;
    }

    /**
     * Execute simple query with prepared statement.
     * @param $statement
     * @param $attributes
     * @return PDOStatement
     */
    public function querySimplePrepare($statement, $attributes) {
        $req = $this->getPDO()->prepare($statement);
        $req->execute($attributes);
        return $req;
    }


    /**
     * Execute simple query with prepared statement with limit.
     * @param $statement
     * @param $attributes
     * @return bool|PDOStatement
     */
    public function queryLimitPrepare($statement, $attributes) {
        $req = $this->getPDO()->prepare($statement);
        foreach ($attributes as $k => $v) {
            if (is_int($v)) {
                $req->bindValue($k + 1, intval($v), PDO::PARAM_INT);
            } else {
                $req->bindValue($k + 1, intval($v), PDO::PARAM_STR);
            }
        }
        $req->execute();
        return $req;
    }


    /**
     * Execute prepared query with return object.
     * @param $statement
     * @param $attributes
     * @return bool|mixed
     */
    public function prepareReturn($statement, $attributes) {
        try {
            $req = $this->getPDO()->prepare($statement);
            $req->execute($attributes);
            $datas = $req->fetch(PDO::FETCH_OBJ);

            return $datas;
        } catch (\PDOException $e) {
            echo $e;
            return false;
        }
    }

    /**
     * Check if the request is null.
     * @param $statement
     * @param $attributes
     * @return bool
     */
    public function isNull($statement, $attributes) {
        try {
            $req = $this->getPDO()->prepare($statement);
            $req->execute($attributes);

            if ($req->rowCount() === 0) {
                return true;
            } else {
                return false;
            }

        } catch (\PDOException $e) {
            return true;
        }
    }

    /**
     * Check if the request is null.
     * @param $statement
     * @param $attributes
     * @return bool
     */
    public function isNullSimple($statement) {
        try {
            $req = $this->getPDO()->query($statement);

            if ($req->rowCount() === 0) {
                return true;
            } else {
                return false;
            }

        } catch (\PDOException $e) {
            return true;
        }
    }

    /**
     * Get the count of a query.
     * @param $statement
     * @param $attributes
     * @return bool|int
     */
    public function count($statement, $attributes) {
        try {
            $req = $this->getPDO()->prepare($statement);
            $req->execute($attributes);

            return $req->rowCount();
        } catch (\PDOException $e) {
            return true;
        }
    }

    /**
     * Return last id entered.
     * @return string
     */
    public function lastId() {
        return $this->getPDO()->lastInsertId();
    }

}
