<?php

namespace App\TwigExtension;

use App\User\UserManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class TwigExtension extends AbstractExtension
{
    private $userManager;

    public function __construct()
    {
        $this->userManager = new UserManager();
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('isOnline', [$this, 'isOnline']),
            new TwigFunction('user', [$this, 'user']),
        ];
    }

    public function isOnline()
    {
        if (isset($_SESSION["auth"])){
            return true;
        }else{
            return false;
        }
    }

    public function user()
    {
        if ($this->isOnline()){
            return $this->userManager->findById($_SESSION["auth"]["id"]);
        }
    }
}