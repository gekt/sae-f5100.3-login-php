<?php

namespace App\Router;

use App\Utils\Utils;

class Router
{

    private $url;
    private Utils $utils;
    private bool $found = false;

    public function __construct()
    {
        $this->url = $_SERVER["REQUEST_URI"];
        $this->utils = new Utils();
    }

    public function isConnected($connected){
        if ($connected !== NULL){
            if ($connected && !isset($_SESSION["auth"])){
                header("Location: /login");
                exit();
            }elseif (!$connected && isset($_SESSION["auth"])){
                header("Location: /");
                exit();
            }
        }
    }

    public function get($route,$callback,$requireAuth = NULL){
        $this->route($route, $callback,"GET",$requireAuth);
    }

    public function post($route,$callback,$requireAuth = NULL){
        $this->route($route,$callback,"POST",$requireAuth);
    }

    public function route($route, $callback, $method,$requireAuth){
        if ($_SERVER["REQUEST_METHOD"] !== $method){
            return;
        }

        $pattern = "";

        $patternList = explode("/",$route);
        $requestPatternList = explode("/",$this->url);

        array_splice($patternList, 0,1);
        array_splice($requestPatternList,0,1);


        foreach ($patternList as $k => $v){
            $pattern .= "\/(".preg_replace("/(\{.*\})/",".*",$v).")";
        }

        if(preg_match("/^".$pattern."$/i",$this->url)){
            $this->isConnected($requireAuth);
            $this->found = true;
            $args = [];

            foreach ($patternList as $k => $v){
                if (preg_match("/\{(.*)\}/i",$v,$matches)){
                    $args[$matches[1]] = $requestPatternList[$k];
                }
            }

            call_user_func($callback,$args);
        }
    }

    public function notFound($callback){
        if (!$this->found){
            call_user_func($callback);
        }
    }
}