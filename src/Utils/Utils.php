<?php

namespace App\Utils;

class Utils
{
    public function dump($toDump){
        echo "<pre>";
        var_dump($toDump);
        echo "</pre>";
    }

    /**
     * This function is used to display flash message.
     */
    public
    function alert() {
        if (isset($_SESSION['flash'])):?>
            <?php foreach ($_SESSION['flash'] as $type => $message): ?>
                <?php $class = ''; ?>
                <?php if (isset($_SESSION['flash']['success'])) {
                    $class = 'toast-success';
                } ?>
                <?php if (isset($_SESSION['flash']['error'])) {
                    $class = 'toast-error';
                } ?>
                <script type='text/javascript'> setTimeout(function () {
                        M.toast({html: "<?= $message; ?>", classes: '<?= $class ?>'});
                    }, 200)</script>
            <?php endforeach; ?>
            <?php unset($_SESSION['flash']); ?>
        <?php endif;
    }

}