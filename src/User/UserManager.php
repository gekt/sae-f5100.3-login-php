<?php

namespace App\User;

use App\Database\Database;

class UserManager
{
    private Database $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    /**
     * This function is used to find and get object of an user
     * @param $id
     * @return User|bool
     */

    public function findById($id){
        $user = $this->db->prepareReturn('SELECT * FROM users WHERE id= ?', [$id]);
        if (!$user) return false;
        return new User($user->id, $user->username, $user->password, $user->email, $user->attempt);
    }

    /**
     * This function is used to find and get object of an user
     * @param $username
     * @return User|bool
     */
    public function findByUsername($username)
    {
        $user = $this->db->prepareReturn('SELECT * FROM users WHERE username= ?', [$username]);
        if (!$user) return false;
        return new User($user->id, $user->username, $user->password, $user->email, $user->attempt);
    }


    /**
     * Function used to login an user.
     * @param $data
     */
    public function login($data){
        $username = $data["username"];
        $password = $data["password"];

        if (!$this->findByUsername($username)){
            $arrErrors["username"] = "Utilisateur inexistant !";

            header('HTTP/1.1 400 Method not allowed');
            echo json_encode([
                "error" => true,
                "message" => [
                    "username" => "Utilisateur innexistant !"
                ]
            ]);
        }else{

            $user = $this->findByUsername($username);

            if ($user->getAttempt() < 3){
                if (password_verify($password,$user->getPassword())){

                    $_SESSION["auth"] = [
                        "id" => $user->getId(),
                        "username" => $user->getUsername()
                    ];

                    $_SESSION["flash"]["success"] = "Vous êtes connecté !";

                    $user->setAttempt(0);
                    $user->update();

                    echo json_encode([
                        "error" => false,
                        "message" => [
                            "connect" => "Connecté !"
                        ]
                    ]);
                }else{
                    header('HTTP/1.1 400 Method not allowed');
                    echo json_encode([
                        "error" => true,
                        "message" => [
                            "password" => "Mauvais mot de passe !"
                        ]
                    ]);

                    $calcAttempt = $user->getAttempt() + 1;
                    $user->setAttempt($calcAttempt);
                    $user->update();
                }
            }else{
                header('HTTP/1.1 400 Method not allowed');
                echo json_encode([
                    "error" => true,
                    "message" => [
                        "connect" => "Compte bloqué trop d'essaies !"
                    ]
                ]);
            }
        }
    }

    /**
     * Function used to register an user.
     * @param $data
     */
    public function register($data){
        $username = $data["username"];
        $email = $data["email"];
        $password = $data["password"];
        $passwordConfirm = $data["passwordConfirm"];

        $arrErrors = [];


        if (!$this->findByUsername($username)){
            // Check field if correct
            foreach ($data as $k => $v) {

                if ($k === "email"){
                    if (!filter_var($v,FILTER_VALIDATE_EMAIL)){
                        $arrErrors[$k] = "Email incorrect !";
                    }
                }

                if (empty($v)) {
                    $arrErrors[$k] = "Veuillez remplir le champs correctement !";
                }
            }

            //Check if password are equals
            if ($passwordConfirm !== $password){
                $arrErrors["password"] = "Les mots de passe ne corresponent pas !";
                $arrErrors["passwordConfirm"] = "Les mots de passe ne corresponent pas !";
            }else{
                $passHash = password_hash($password,PASSWORD_BCRYPT);
            }

            //Check if there is errors
            if (count($arrErrors) > 0){
                //Errors detected returns to user
                header('HTTP/1.1 400 Method not allowed');
                echo json_encode([
                    "error" => true,
                    "message" => $arrErrors
                ]);
            }else{
                //Register user
                $this->db->querySimplePrepare("INSERT INTO users SET username= ?, password=?, email=?", [
                    $username,
                    $passHash,
                    $email
                ]);

                $_SESSION["auth"] = [
                    "id" => $this->db->lastId(),
                    "username" => $username
                ];

                $_SESSION["flash"]["success"] = "Vous êtes connecté !";

                echo json_encode([
                    "error" => false,
                    "message" => "Formulaire envoyé !"
                ]);
            }
        }else{
            //user exist
            header('HTTP/1.1 400 Method not allowed');
            echo json_encode([
                "error" => true,
                "message" => [
                    "username" => "Utilisateur déjà existant !"
                ]
            ]);
        }
    }


    /**
     * This function is used to logut the user.
     */
    public function logout() {
        session_unset();
        session_destroy();
        unset($_SESSION['auth']);
        session_start();
        $_SESSION['flash']['success'] = "Vous êtes bien déconnecté !";
        header('location: /');
        exit;
    }

}