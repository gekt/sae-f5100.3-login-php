<?php

namespace App\User;

use App\Database\Database;

class User
{
    private int $id;
    private string $username;
    private string $password;
    private string $email;
    private int $attempt;

    private Database $db;

    /**
     * @param int $id
     * @param string $username
     * @param string $password
     * @param string $email
     * @param int $attempt
     */
    public function __construct(int $id, string $username, string $password, string $email, int $attempt)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
        $this->email = $email;
        $this->attempt = $attempt;
        $this->db = new Database();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getAttempt(): int
    {
        return $this->attempt;
    }

    /**
     * @param int $attempt
     */
    public function setAttempt(int $attempt): void
    {
        $this->attempt = $attempt;
    }

    /**
     * This function is used to update the user in the database trought the object
     */
    public function update(){
        $this->db->querySimplePrepare('UPDATE users SET username=?, password=?,email=?,attempt=? WHERE id=?',[
            $this->username,
            $this->password,
            $this->email,
            $this->attempt,
            $this->id,
        ]);
    }

    /**
     * This function is used to delete an user
     */
    public function delete(){
        $this->db->querySimplePrepare('DELETE FROM users WHERE id =?', [
            $this->id
        ]);
    }



}