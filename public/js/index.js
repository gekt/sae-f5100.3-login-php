const formRegister = document.getElementById("registerForm");
const formLogin = document.getElementById("loginForm");


/**
 * Login form send
 */

async function loginForm(e){
        e.preventDefault();

        const body = new FormData(formLogin);
        body.append("loginForm", "loginForm");

        const errors = document.getElementsByClassName("status-text");

        Array.from(errors).forEach((el) => {
            el.textContent = "";
        });


        const req = await fetch("/post/login", {
            method: "POST",
            headers: {
                "Accept": "application/json"
            },
            body
        })

        const res = await req.json();

        if (req.status === 400) {
            Object.keys(res.message).forEach((e) => {
                console.log(e, res.message[e]);
                document.getElementById(e + "-status").textContent = res.message[e];
            })
        } else {
            window.location.href = "/";
        }
}


/**
 * Register event send form
 */

async function registerForm(e){
    e.preventDefault();

    const body = new FormData(formRegister);
    body.append("registerForm","registerForm");

    const errors = document.getElementsByClassName("status-text");

    Array.from(errors).forEach((el) => {
        el.textContent = "";
    });

    const req = await fetch("/post/register", {
        method: "POST",
        headers: {
            "Accept": "application/json"
        },
        body
    })

    const res = await req.json();

    if (req.status === 400){
        Object.keys(res.message).forEach((e) => {
            console.log(e,res.message[e]);
            document.getElementById(e+"-status").textContent = res.message[e];
        })
    }else{
        window.location.href = "/";
    }
}