<?php

require "../vendor/autoload.php";

session_start();

use App\Router\Router;
use App\User\UserManager;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use App\Utils\Utils;

$router = new Router();

// Specify our Twig templates location
$loader = new FilesystemLoader(__DIR__.'/../templates/');

// Instantiate our Twig
$twig = new Environment($loader);

$twig->addExtension(new \App\TwigExtension\TwigExtension());

//Instantiate Utils
$utils = new Utils();

if ($_SERVER['REQUEST_METHOD'] === "GET"){
    $utils->alert();
}

//GET ROUTES
$router->get("/", function () use ($twig){
    echo $twig->render('home.html.twig');
});

$router->get("/login", function () use ($twig){
    echo $twig->render('login.html.twig');
},false);

$router->get("/register", function () use ($twig){
    echo $twig->render('register.html.twig');
},false);

$router->get("/logout", function () {
    $userManager = new UserManager();
    $userManager->logout();
},true);

//POST ROUTES

$router->post("/post/login", function () {
    $userManager = new UserManager();
    $userManager->login($_POST);
},false);

$router->post("/post/register", function () {
    $userManager = new UserManager();
    $userManager->register($_POST);
},false);

//Not found
$router->notFound(function (){
    echo "404";
});



